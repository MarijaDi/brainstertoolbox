$('.loginButton').click(function(e) {
    e.preventDefault();

    $('#menuModal')
        .modal('hide')
        .on('hidden.bs.modal', function(e) {
            $('#menuLogin').modal('show');

            $(this).off('hidden.bs.modal');
        });

});

$('.registerButton').click(function(e) {
    e.preventDefault();

    $('#menuModal')
        .modal('hide')
        .on('hidden.bs.modal', function(e) {
            $('#menuRegister').modal('show');

            $(this).off('hidden.bs.modal');
        });

});
$('.aboutUsButton').click(function(e) {
    e.preventDefault();

    $('#menuModal')
        .modal('hide')
        .on('hidden.bs.modal', function(e) {
            $('#aboutUs').modal('show');

            $(this).off('hidden.bs.modal');
        });

});
$('.contactButton').click(function(e) {
    e.preventDefault();

    $('#menuModal')
        .modal('hide')
        .on('hidden.bs.modal', function(e) {
            $('#contactModal').modal('show');

            $(this).off('hidden.bs.modal');
        });

});