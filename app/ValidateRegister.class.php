<?php 

require_once '../public/bootstrap.php';


class ValidateRegister {
    private $email;
    private $name;
    private $company;
    private $phone_number;
    private $sector_id;
    private $password;
    private $passwordConfirm;
    private $number_of_employees;
    private $message;
    private $error = [];
    private $input = [];
    private $table;

    public function __construct(){
        $this->email = $_POST['email'];
        $this->name = $_POST['name'];
        $this->company = $_POST['company'];
        $this->phone_number = $_POST['phone_number'];
        $this->sector_id = $_POST['sector_id'];
        $this->password = $_POST['password'];
        $this->passwordConfirm = $_POST['passwordConfirm'];
        $this->number_of_employees = $_POST['number_of_employees_id'];
        $this->message = $_POST['message'];
    }

    public function validation(){
        $email = $this->emailExistance();
        $reqField = ['email' => $this->email, 'name' => $this->name, 'password' => $this->password,  'passwordConfirm' => $this->passwordConfirm];
        $reqResult = $this->requiredFields($reqField);
        $pasValidation = $this->passwordValidation();
        $pasMatching = $this->passwordConfirm();

        if($email == true && $reqResult == true && $pasValidation == true && $pasMatching == true){
          return true;
        }
        $this->getInput('email');
        $this->getInput('name');
        $this->getInput('company');
        $this->getInput('phone_number');
        $this->getInput('message');
        $inputs = implode($this->input, '&');
        header("Location:../public/index.php?modal=openRegister&{$this->getError()}&$inputs");
        die();
    }

    public function setTable($table){
        $this->table = $table;
    }
    private function getTable(){
        return $this->table;
    }
    private function setError(string $key, string $error){
        array_push($this->error, "$key=$error");
    }
    private function getError(){
       $arr = implode("&", $this->error);
       return $arr;
    }

    private function emailExistance(){
        global $query;
        $ex = $query->findWithEmail('admin', $this->email);
        if($ex){
           return $this->setError('emailError', 'existantAccount');
        }
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
           return $this->setError('emailError', 'invalidEmail');
        }
        return true;
    }
    private function requiredFields(array $ar){
        $i = 0;
        foreach ($ar as $value => $input){
            if($input === '' || !isset($input)){
              $this->setError($value, 'required');
              $i++;
            }
        }
       if($i==0){
           return true;
       } return false;
    }
    private function passwordValidation()
    {
        $specialChar = false;
        $upper = false;
        $number = false;
        $success = false;
        if (preg_match('/[\'^£$%&*()}{@#~?>!<>,|=_+¬-]/', $this->password)) {
            $specialChar = true;
        }
        if (preg_match("/[A-Z]/", $this->password)) {
            $upper = true;
        }
        if (preg_match("/[0-9]/", $this->password)) {
            $number = true;
        }
        if(!$specialChar || !$upper || !$number){
           return $this->setError('password', 'charsNotValid');
        }
        return true;
    }
    private function passwordConfirm()
    {
        if ($this->password !== $this->passwordConfirm) {
            return $this->setError('passwordMatch', 'passwordsDoNotMatch');
        }
        return true;
    }
    private function getInput($value){
        if(isset($this->$value) && $this->$value !== ""){
            $try = $this->$value;
            array_push($this->input, $value."Input=$try");}
    }
}
