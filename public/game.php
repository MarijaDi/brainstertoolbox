<?php 
require_once '../app/targetemail.php';

$log = "Најави се";
$flagLogin = 0;
if(isset($_SESSION['emailLogin'])){
   $flagLogin = 1;
   $user = $query->findWithEmail('admin', $_SESSION['emailLogin']);         
   $username = $user->name;
   $log = "Одјави се";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
        integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?=$card->title?></title>
</head>

<body id="home" class='bodyGame'>
    <?php include_once('layout/navbar.php');?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 headerPhotoColumn">
                <img src='<?= $card->card_photo?>' alt="" srcset="" class="headerPhoto">
            </div>
        </div>
        <div class="row contentGamePage">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                <div class="row titlePage">
                    <div class="col-md-12 col-sm-12 titleColumnDiv"><?=$card->title?></div>
                </div>
                <div class="row informationPage">
                    <div class="col-md-3 col-sm-6 col-xs-6 seperateInformationPage">
                        <div class="row infoGamePageMainDiv">
                            <div class="col-md-1 col-sm-2 col-xs-2 infoGamePageGlyph">
                                <span class="glyphicon glyphicon-time glyphiconGamePage" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-9 infoGamePageDiv">
                                <p class="infoGamePageParagraph">Time Frame</p>
                                <p class="infoGamePageParagraphInput"><?=$card->time_frame?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 seperateInformationPage">
                        <div class="row infoGamePageMainDiv">
                            <div class="col-md-1 col-sm-2 col-xs-2 infoGamePageGlyph">
                                <span class="glyphicon glyphicon-user glyphiconGamePage" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-9 infoGamePageDiv">
                                <p class="infoGamePageParagraph">Group Size</p>
                                <p class="infoGamePageParagraphInput"><?=$card->group_size?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 seperateInformationPage">
                        <div class="row infoGamePageMainDiv">
                            <div class="col-md-1 col-sm-2 col-xs-2 infoGamePageGlyph">
                                <i class="fa fa-university cardFontAwesomeIcon" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-9 infoGamePageDiv">
                                <p class="infoGamePageParagraph">Facilitation Level</p>
                                <p class="infoGamePageParagraphInput"><?=$card->facilitation_level?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 seperateInformationPage">
                        <div class="row infoGamePageMainDiv">
                            <div class="col-md-1 col-sm-2 col-xs-2 infoGamePageGlyph">
                                <span class="glyphicon glyphicon-pencil glyphiconGamePage" aria-hidden="true">
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-9 infoGamePageDiv">
                                <p class="infoGamePageParagraph">Materials</p>
                                <p class="infoGamePageParagraphInput"><?=$card->materials?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row introGamePage">
                    <div class="col-md-12 col-sm-12 explanationColumnDiv"><?=$card->description?></div>
                </div>
                <?php for($i=1; $i<=$card->number_of_steps; $i++){ ?>
                <div class="row step">
                    <div class="col-md-12 col-sm-12 stepColumnDiv">
                        <p class="stepTitle">Чекор <?=$i?></p>
                        <p class="stepExplanation"><?php $step = 'step_'.$i; echo $card->$step;?> </p>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
        <?php include_once('layout/footer.php');?>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
        integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous">
    </script>
    <script src="../assets/modal.js"></script>
    <script>
    <?php openModal(); ?>
    </script>
</body>

</html>