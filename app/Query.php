<?php

class Query
{
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getAll(string $table)
    {
        $statement = $this->pdo->prepare("select * from {$table}");
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_OBJ);
    }

    
    public function find(string $table, int $id)
    {
        $statement = $this->pdo->prepare("select * from {$table} where game_id={$id}");
        $statement->execute();

        return $statement->fetch(PDO::FETCH_OBJ);
    }
    
    public function findWithEmail(string $table, string $email)
    {
        $statement = $this->pdo->prepare("select * from {$table} where email=:email");
        $statement->execute(["email" => $email]);
        return $statement->fetch(PDO::FETCH_OBJ);
    }
    public function insert(string $table, array $parameters)
    {
        $sql = sprintf(
            'insert into %s (%s) values (%s)',
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );

        $this->executePreparedSql($sql, $parameters);
    }

   
    public function update(string $table, int $id, array $parameters)
    {
        $columns = [];
        $keys = array_keys($parameters);

        foreach ($keys as $column) {
            $columns[] = $column . '=:' . $column;
        }

        $sql = sprintf(
            'update %s set %s where %s',
            $table,
            implode(', ', $columns),
            'game_id=' . $id
        );

        $this->executePreparedSql($sql, $parameters);
    }

    public function delete(string $table, int $id)
    {
        $sql = sprintf(
            'delete from %s where %s',
            $table,
            'game_id=' . $id
        );

        $this->executePreparedSql($sql, $parameters);
    }

    protected function executePreparedSql(string $sql, array $parameters)
    {
        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }
}