 <nav class="navbar navbar-fixed-top navbarStyle">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-8 col-sm-6 col-xs-12 navigationMenu">
                 <p><span data-toggle="modal" data-target="#menuModal" class="myMenu"><span
                             class="glyphicon glyphicon-menu-hamburger navbarGlyph" aria-hidden="true"></span><span
                             class="navbarText">Menu</span></span>
                     <?php if($flagLogin == 1){?>
                     <span class="user">
                         <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                         <?= $username?> </span> <?php }?>
                     <a href="index.php" class="marginLogo"><img src="../content/logo.png" alt=""
                             class="navbarLogoPhoto img-center"></a>
                 </p>
             </div>
             <div class="col-md-4 col-sm-6 navbarButtons text-right">
                 <a class="btn btn-default buttonObuka" href="https://www.brainster.io/business​" target="_blank"
                     role="button">Обука за компании</a>
                 <a class="btn btn-default buttonVraboti" href="​https://www.brainster.io/business​" target="_blank"
                     role="button">Вработи наши студенти</a>
             </div>
         </div>
     </div>
 </nav>
 <div class="modal fade modal-p" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog modal-margin" role="document">
         <div class="modal-content modalStyle">
             <div class="modal-body paddingNone">
                 <p class="menuClose">
                     <a href="index.php" class="logoMenu"><img src="../content/logo.png" alt=""
                             class="navbarLogoPhoto img-center"></a>
                     <button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
                         <span class="glyphiconX">X</span></button>
                 </p>
                 <p class="menuItem"><span data-toggle="modal" data-target="#menuRegister"
                         class="registerButton menuButton myMenu">Регистрирај се</span></p>
                 <p class="menuItem"><span data-toggle="modal" data-target="#menuLogin"
                         class="loginButton menuButton myMenu"><?=$log?></span></p>
                 <p class="menuItem"><span data-toggle="modal" data-target="#aboutUs"
                         class="myMenu menuButton aboutUsButton">За нас</span></p>
                 <p class="menuItem"><a href="https://www.facebook.com/pg/brainster.co/photos/"
                         target="_blank">Галерија</a></p>
                 <p class="menuItem"><span data-toggle="modal" data-target="#contactModal"
                         class="contactButton menuButton myMenu">Контакт</span></p>
                 <a class="btn btn-default buttonObukaMobile" href="https://www.brainster.io/business​" target="_blank"
                     role="button">Обука за компании</a>
                 <a class="btn btn-default buttonVrabotiMobile" href="​ https://www.brainster.io/business​"
                     role="button">Вработи наши студенти</a>
             </div>
         </div>
     </div>
 </div>
 <div class="modal fade modal-p" id="menuLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-body">
                 <p class="menuClose closeLogin">
                     <a href="index.php" class="logoMenu"><img src="../content/logo.png" alt=""
                             class="navbarLogoPhoto img-center"></a>
                     <button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
                         <span class="glyphiconX">X</span></button>
                 </p>
                 <?php if($flagLogin == 0) {?>
                 <form action="../app/login.php" method="post">
                     <label for="emailLogin" class="labelLogin">Е-маил</label>
                     <input type="email" name="emailLogin" id="emailLogin" class="inputLogin"
                         value="<?=getEmailLogin()?>">
                     <p class='errorLogin'><?=getErrorEmailLogin()?></p>
                     <label for="passwordLogin" class="labelLogin">Лозинка</label>
                     <input type="password" name="passwordLogin" id="passwordLogin" class="inputLogin">
                     <p class='errorLogin'><?=getErrorPasswordLogin()?></p>
                     <button type="submit" class="buttonLogin">Најави се</button>
                 </form>
                 <?php }elseif ($flagLogin == 1){ ?>
                 <p class="logoutNote">Најавени со следниот е-маил: <?= $_SESSION['emailLogin'];?></p>
                 <a href="../app/logout.php"><button class="buttonLogin">Одјави се</button></a>
                 <?php }?>

             </div>
         </div>
     </div>
 </div>
 <div class="modal fade modal-p" id="menuRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog registerDialog" role="document">
         <div class="modal-content contentRegisterModal">
             <div class="modal-body registerBody">
                 <p class="menuClose closeLogin">
                     <a href="index.php" class="logoMenu"><img src="../content/logo.png" alt=""
                             class="navbarLogoPhoto img-center"></a>
                     <button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
                         <span class="glyphiconX">X</span></button>
                 </p>
                 <div class="row registerFormRow">
                     <div class="col-md-12">
                         <form action="../app/registervalidation.php" method="post">
                             <div class="row">
                                 <div class="col-md-6 col-sm-6">
                                     <label for="name" class="registerLabel">Име <span
                                             class='registerErrorAster'>*</span> </label>
                                     <input type="text" name="name" id="name" class="inputRegister"
                                         value="<?=getInput('name')?>" required>
                                     <p class="registerError"><?=getRequredError('name')?></p>
                                     <label for="email" class="registerLabel">Е-маил адреса <span
                                             class='registerErrorAster'>*</span> </label>
                                     <input type="email" name="email" id="email" class="inputRegister"
                                         value="<?=getInput('email')?>" required>
                                     <p class="registerError"><?=getRequredError('email')?></p>
                                     <p class="registerError"><?=getEmailErrorRegister()?></p>
                                     <label for="password" class="registerLabel">Лозинка <span
                                             class='registerErrorAster'>*</span> </label>
                                     <input type="password" name="password" id="password" class="inputRegister"
                                         required>
                                     <p class="registerError"><?=getPasswordErrorRegister()?></p>
                                     <p class="registerError"><?=getRequredError('password')?></p>
                                     <label for="passwordConfirm" class="registerLabel">Потврдете ја вашата
                                         лозинка <span class='registerErrorAster'>*</span> </label>
                                     <input type="password" name="passwordConfirm" id="passwordConfirm"
                                         class="inputRegister" required>
                                     <p class="registerError"><?=getPasswordMatchErrorRegister()?></p>
                                     <p class="registerError"><?=getRequredError('passwordConfirm')?></p>
                                     <label for="phone_number" class="registerLabel">Телефонски број</label>
                                     <input type="text" name="phone_number" id="phone_number" class="inputRegister"
                                         value="<?=getInput('phone_number')?>">
                                 </div>
                                 <div class="col-md-6 col-sm-6">
                                     <label for="company" class="registerLabel">Компанија</label>
                                     <input type="text" name="company" id="company" class="inputRegister"
                                         value="<?=getInput('company')?>">
                                     <label for="sector_id" class="registerLabel">Селектирајте сектор на
                                         работа</label>
                                     <select name="sector_id" id="sector_id" class="inputRegister selectRegister">
                                         <option value="">Сектор на работа</option>
                                         <option value="1">Човечки ресурси</option>
                                         <option value="2">Маркетинг</option>
                                         <option value="3">Продукт</option>
                                         <option value="4">Продажба</option>
                                         <option value="5">СЕО</option>
                                         <option value="6">друго</option>
                                     </select>
                                     <label for="number_of_employees" class="registerLabel">Селектирајте број на
                                         вработени</label>
                                     <select name="number_of_employees_id" id="number_of_employees"
                                         class="inputRegister selectRegister">
                                         <option value="">Број на вработени во компанијата</option>
                                         <option value="1">1</option>
                                         <option value="2">2-10</option>
                                         <option value="3">11-50</option>
                                         <option value="4">50-200</option>
                                         <option value="5">200+</option>
                                     </select>
                                     <label for="message" class="registerLabel">Порака</label>
                                     <textarea name="message" id="message"
                                         class="inputRegister registerTextArea"><?=getInput('message')?></textarea>
                                 </div>
                                 <p class='registerError'>Полињата означени со * се задолжителни</p>
                                 <p class="text-center"><button type="submit"
                                         class="buttonRegisterSubmit">Потврди</button></p>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>