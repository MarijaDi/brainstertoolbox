<?php

interface Connection
{
    public function connect(): PDO;
}
