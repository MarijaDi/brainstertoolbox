<?php 
if($_SERVER['REQUEST_METHOD'] != 'POST'){
    header("Location:../public/index.php");
    die();
}

require_once '../public/bootstrap.php';
    session_start();

    $email = $_POST['emailLogin'];
    $tableName = 'admin';

    $emailencrypt = openssl_encrypt($email, 'AES-128-ECB', "12345678pslg");
    urlencode($emailencrypt);

    $find = $query->findWithEmail($tableName, $email);
    if($find === false){
        header("Location:../public/index.php?error=noLoginAccount&emailLogin={$emailencrypt}&modal=openLogin");
        die();
    }

    $password = md5($_POST['passwordLogin']);

    if($password == $find->password){
        $_SESSION['emailLogin'] = $email;
        header('Location:../public/index.php');
        die();
    }
    
    header("Location:../public/index.php?error=wrongLoginPassword&emailLogin={$emailencrypt}&modal=openLogin");
    die();
    