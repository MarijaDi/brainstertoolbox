<?php

function emailValidation($email)
    {
        $success = false;
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $success = true;
        }
        
        return $success;
}

function getScript(){
    if(isset($_GET['modal'])){
        $id = $_GET['modal'];
        $id = openssl_decrypt($id, 'AES-128-ECB', "idsend");
        echo "$('#cardModal$id').modal('show')\n";
    }
    else echo "";
}

function getErrorMail(){
    if(isset($_GET['error']) && $_GET['error'] == 'notAValidEmail'){        
        return "Внесете валидна емаил адреса";
    } else return;
}

function openModal(){
    if(isset($_GET['modal']) && $_GET['modal'] == 'openLogin'){
        echo "$('#menuLogin').modal('show')\n";
    } else if(isset($_GET['modal']) && $_GET['modal'] == 'openRegister'){
        echo "$('#menuRegister').modal('show')\n";
    }
    else echo "";
}

function getEmailLogin(){
    if(isset($_GET['emailLogin'])){
        $emailencrypt = $_GET['emailLogin'];
        urldecode($emailencrypt);
        $emaildecrypt = openssl_decrypt($emailencrypt, 'AES-128-ECB', "12345678pslg");
        return $emaildecrypt;       
    }
}

function getErrorPasswordLogin(){
    if(isset($_GET['error']) && $_GET['error'] == 'wrongLoginPassword')
        return 'Погрешна лозинка';
    else return;
}
function getErrorEmailLogin(){
    if(isset($_GET['error']) && $_GET['error'] == 'noLoginAccount')
        return 'Нема профил со оваа емаил адреса.';
    else return;
}

function getEmailErrorRegister(){
    if(isset($_GET['emailError'])){
        if($_GET['emailError'] == 'existantAccount')
            return 'Постои профил со оваа емаил адреса';
        if($_GET['emailError'] == 'invalidEmail')
            return 'Ве молиме внесете валидна емаил адреса';
    }
    else return;
}

function getRequredError($name){
    if(isset($_GET[$name]) && $_GET[$name] == 'required'){
        return 'Полето е задолжително';
    }
    else return;
}

function getPasswordErrorRegister(){
    if(isset($_GET['password']) && $_GET['password'] == 'charsNotValid')
        return 'Лозинката мора да содржи специјален карактер, голема буква, и бројка';
    else return;
}
function getPasswordMatchErrorRegister(){
    if(isset($_GET['passwordMatch']) && $_GET['passwordMatch'] == 'passwordsDoNotMatch')
        return 'Лозинките не се совпаѓаат';
    else return;
}

function getInput($value){
    $input = $value."Input";
    if(isset($_GET[$input]))
        return $_GET[$input];
    else return;
}