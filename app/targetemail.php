<?php

require "bootstrap.php";
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $email = $_POST["email"];
    if(emailValidation($email) === false){
        header('Location:index.php?error=notAValidEmail&modal='.$_GET['id']);
    } 
    if($query->findWithEmail("targeted_visitors", $email) === false){
        $query->insert('targeted_visitors', $_POST);       
    }
    $visitor = $query->findWithEmail("targeted_visitors", $email);
    $emailSession = $visitor->email;
    $_SESSION['emailTarget'] = $emailSession;
}

if(!isset($_GET['id'])){
    header("Location:index.php?error=noID");
}
$id = $_GET['id'];
$id = openssl_decrypt($id, 'AES-128-ECB', "idsend");
$card = $query->find('cards', $id);
