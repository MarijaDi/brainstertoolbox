<div class="row footerBackground">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 text-center futureProof">
                        <p class="futureProofPOne">Future-proof your organization</p>
                        <p class="futureProofPTwo">Find out how to unlock progress in your business. Understand your
                            current
                            state, identify opportunity areas and prepare to take charge of your organization's future.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center buttonBottom">
                    <a href="http://www.brainsterquiz.typeform.com/to/kC2I9E" class="btn btn-default buttonAssesment" target="_blank" role="button">Take
                            the assessment</a>
                    </div>
                </div>
                <div class="row footer">
                    <div class="col-md-2 col-md-offset-1 col-sm-3 col-xs-6 footerLinksDiv">
                        <p class="footerLinks"><span data-toggle="modal" data-target="#aboutUs" class="">About Us</span>
                        </p>
                        <p class="footerLinks"><span data-toggle="modal" data-target="#contactModal"
                                class="">Contact</span></p>
                        <p class="footerLinks"><a href="https://www.facebook.com/pg/brainster.co/photos/"
                                target="_blank">Gallery</a></p>
                    </div>
                    <div class="col-md-6 col-sm-6 text-center footerLogoDiv">
                        <p><a href="index.php"><img src="../content/logo.png" alt=""
                                    class="navbarLogoPhotoBottom img-center"></p>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-6 text-right footerIconDiv">
                        <a href="https://www.linkedin.com/school/brainster-co/" target="_blank"><i
                                class="fa fa-linkedin footerIcon"></i></a>
                        <a href="https://www.twitter.com/brainsterco/" target="_blank"><i
                                class="fa fa-twitter footerIcon"></i></a>
                        <a href="https://www.facebook.com/brainster.co/" target="_blank"><i
                                class="fa fa-facebook-f footerIcon"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-center footerLogoDivMobile">
                        <p><a href="index.php"><img src="../content/logo.png" alt=""
                                    class="navbarLogoPhotoBottom img-center"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade modal-p" id="aboutUs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog aboutUsDialog" role="document">
                <div class="modal-content">
                    <div class="modal-body aboutUsBody">
                        <p class="menuClose closeLogin">
                            <a href="index.php" class="logoMenu"><img src="../content/logo.png" alt=""
                                    class="navbarLogoPhoto img-center"></a>
                            <button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
                                <span class="glyphiconX">X</span>
                                Затвори</button>
                        </p>
                        <p class='aboutUsContent'>Brainster е иновативна компанија за едукација каде можеш да научиш
                            практични вештини за
                            трансформација на кариерата. Нашата специјалност е организирање на курсеви, академии,
                            кариерна
                            трансформација и поврзување на талентите со иновативните компании oд областа на маркетинг,
                            дизајн, бизнис и програмирање.</p>

                        <p class='aboutUsContent'>Во изминатите 3 години Brainster реализираше повеќе од 400 курсеви
                            преку кои 5000 учесници се
                            стекнаа со нови вештини и стигнаа чекор поблиску до кариерата која ја посакуваат.
                            Нашата мисија е да им помогнеме на 1 милион индивидуалци да стигнат до кариера која секогаш
                            ја
                            посакувале.</p>
                        <ul class="aboutusUl">
                            <li>Курсеви: <a href="https://brainster.co/courses">https://brainster.co/courses</a></li>
                            <li>Академија за маркетинг: <a
                                    href="https://marketpreneurs.brainster.co">https://marketpreneurs.brainster.co/</a>
                            </li>
                            <li>Академија за дизајн: <a
                                    href="https://design.brainster.co">https://design.brainster.co/</a></li>
                            <li>Академија за програмирање: <a href="http://codepreneurs.co">http://codepreneurs.co/</a>
                            </li>
                            <li> Академија за Data Science: <a
                                    href="https://datascience.brainster.co">https://datascience.brainster.co/</a></li>
                            <li>Академија за Software testing: <a
                                    href="https://qa.brainster.co">https://qa.brainster.co/</a></li>
                            <li>Академија за UX/UI Design: <a
                                    href="https://ux.brainster.co">https://ux.brainster.co/</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade modal-p" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog dialogContact" role="document">
                <div class="modal-content">
                    <div class="modal-body contactBody">
                        <p class="menuClose closeLogin">
                            <a href="index.php" class="logoMenu"><img src="../content/logo.png" alt=""
                                    class="navbarLogoPhoto img-center"></a>
                            <button type="button" class="close closeButton" data-dismiss="modal" aria-label="Close">
                                <span class="glyphiconX">X</span>
                                Затвори</button>
                        </p>
                        <p class="contactTitle">Contact Brainster</p>
                        <ul class="contactUl">
                            <li>Address: <a
                                    href="https://www.google.com/maps/dir/41.3430157,21.5826145/brainster/@41.6741331,21.4351266,10z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x13541449c116c037:0x88831c22d3d17fb2!2m2!1d21.4193517!2d41.9924438"
                                    target="_blank"> Brainster Space ul MK, Vasil Gjorgov 19, Skopje 1000</a></li>
                            <li>Phone: <a href="tel:+070384728">070 384 728</a></li>
                            <li>Website: <a href="http://brainster.co" target="_blank">https://brainster.co/</a></li>
                            <li>E-mail address: <a href="mailto:contact@brainster.co"
                                    target="_blank">contact@brainster.co</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>