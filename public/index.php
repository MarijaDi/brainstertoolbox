<?php
require ('bootstrap.php');

$log = "Најави се";
$flag = 0;
$flagLogin = 0;
session_start();
if(isset($_SESSION['emailLogin'])){
   $flagLogin = 1;
   $flag = 1;
   $user = $query->findWithEmail('admin', $_SESSION['emailLogin']);         
   $username = $user->name;
   $log = "Одјави се";
} else if(isset($_SESSION['emailTarget'])){
   $flag = 1;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta charset="utf8_general_ci">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
        integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/style.css">

    <title>Brainster ToolBox</title>
</head>

<body id="home">
    <?php include_once('layout/navbar.php');?>

    <div class="container-fluid">
        <div class="row headerBackground">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 text-center futureProof futureHeader">
                        <p class="futureProofPOne">Future-proof your organization</p>
                        <p class="futureProofPTwo">Find out how to unlock progress in your business. Understand your
                            current
                            state, identify opportunity areas and prepare to take charge of your organization's future.
                        </p>
                    </div>
                </div>

                <div class="row assesmentRow">
                    <div class="col-md-12 text-center">
                            <a href="http://www.brainsterquiz.typeform.com/to/kC2I9E" class="btn btn-default buttonAssesment" target="_blank" role="button">Take
                            the assessment</a>
                    </div>
                </div>
                <div class="row filterCardM" id="filterMobile">
                    <span class="glyphicon glyphicon-filter filterCardsGlyph" aria-hidden="true"></span>
                    Filter games
                </div>
                <div class="row cardFilter">
                    <div class="row">
                        <div class="col-md-12 closeFilters">
                            Close
                        </div>
                    </div>
                    <div class="col-md-10 col-md-offset-1 col-sm-12 columnCardFilters">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 categoryColumn">
                                <div class="row">
                                    <p class="titleFilter">Browse by category</p>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 categoryFilter filterSelect"
                                                id="energizers">
                                                Energizers
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 categoryFilter filterSelect" id="action">
                                                Action
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 categoryFilter filterSelect" id="inovation">
                                                Inovation
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 categoryFilter filterSelect" id="team">
                                                Team
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 categoryFilter filterSelect"
                                                id="leadership">
                                                Self-leadership
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 categoryFilter filterSelect" id="selectAll">
                                                All
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12 timeColumn">
                                        <div class="row">
                                            <p class="titleFilter">Time frame (minutes)</p>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-12 timeFilter filterSelect" id="timeOne">
                                                        5-30
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 timeFilter filterSelect" id="timeThree">
                                                        60-120
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-12  timeFilter filterSelect" id="timeTwo">
                                                        30-60
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 timeFilter filterSelect" id="timeFour">
                                                        120-240
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12 groupColumn">
                                        <div class="row">
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <div class="row">
                                                    <p class="titleFilter">Group Size</p>
                                                    <div class="col-md-4 col-sm-4 col-xs-12 groupFilter filterSelect"
                                                        id="groupOne">
                                                        2-10
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-xs-12 groupFilter filterSelect"
                                                        id="groupTwo">
                                                        10-40
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-xs-12 groupFilter filterSelect"
                                                        id="groupThree">
                                                        40+
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12 searchColumn" id="searchFilters">
                                                Search
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row cards">
                    <div class="col-md-10 col-md-offset-1">
                        <?php for($i=1; $i<=18; $i++){ 
                    $card = $query->find('cards', $i);
                    $u = openssl_encrypt($i, 'AES-128-ECB', "idsend");
                    $u = urlencode($u);
                    $cardmodal = 'cardModal'.$i;?>
                        <?php if($flag == 1) {?> <a href="game.php?id=<?=$u?>"> <?php }?>
                            <div class="col-md-4 col-sm-6 col-xs-12 card <?=$card->categoryClass?> <?=$card->timeClass?> <?=$card->groupClass?>"
                                data-toggle="modal" data-target="#<?=$cardmodal?>">
                                <div class="row cardRow">
                                    <div class="col-md-12 cardPhotoColumn">
                                        <img src="<?= $card->card_photo?>" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-sm-9 col-xs-8 cardText">
                                            <p class="cardTitle">
                                                <?= $card->title?>
                                            </p>
                                            <p class="cardCategory">Категорија: <span class="cardCategorySpan">
                                                    <?= $card->category?></span>
                                            </p>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-4 cardIconDiv">
                                            <img src="<?= $card->game_icon?>" alt="" class="cardIcon img-circle">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 cardTime">
                                            <div class="row">
                                                <div class="col-md-1 col-sm-1 col-xs-2"> <span
                                                        class="glyphicon glyphicon-time cardGlyphicon"
                                                        aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-xs-9 cardTimeframeText">
                                                    <p class="cardTimeFrame">Времетраење</p>
                                                    <p class="cardTimeFrameInput">
                                                        <?= $card->time_frame?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <?php if($flag == 1) {?>
                        </a> <?php }?>
                        <?php if($flag==0){?>
                        <div class="modal fade modalNone" id="<?=$cardmodal?>" tabindex="-1" role="dialog"
                            aria-labelledby="cardModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content cardModalMainDiv">
                                    <div class="modal-body cardModalBodyDiv">
                                        <p class="modalCardBodyText">
                                            Внесете ја вашата е-маил адреса за да продолжите</p>
                                        <form action="game.php?id=<?=$u?>" method="POST">
                                            <input type="email" name="email" id="emailModal" class="emailCardModal"
                                                placeholder="Вашата е-маил адреса">
                                            <p class='error'><?= getErrorMail();?></p>
                                            <button type="submit" class="buttonCardModal">Продолжи</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><?php }?>
                        <?php }?>
                    </div>
                </div>
                <div class="row" id="noCards">
                    <div class="col-md-12">
                        <p class="noGames">Нема игра која ги пополнува вашите барања</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row goOnTop">
            <div class="col-mg-12">
                <a href="#home"> <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a>
            </div>
        </div>
        <?php include_once('layout/footer.php');?>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
        integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous">
    </script>
    <script src="../assets/filters.js"></script>
    <script src="../assets/modal.js"></script>
    <script>
    <?php openModal();
    getScript(); ?>
    </script>

</body>

</html>