let actionAll = document.querySelectorAll('.action')
let inovationAll = document.querySelectorAll('.inovation')
let teamAll = document.querySelectorAll('.team')
let leadershipAll = document.querySelectorAll('.leadership')
let energizersAll = document.querySelectorAll('.energizers')
let allCategories = document.querySelectorAll('.card')
let timeOneAll = document.querySelectorAll('.timeOne')
let timeTwoAll = document.querySelectorAll('.timeTwo')
let timeThreeAll = document.querySelectorAll('.timeThree')
let timeFourAll = document.querySelectorAll('.timeFour')
let groupOneAll = document.querySelectorAll('.groupOne')
let groupTwoAll = document.querySelectorAll('.groupTwo')
let groupThreeAll = document.querySelectorAll('.groupThree')

var actionClicked = false
var inovationClicked = false
var teamClicked = false
var leadershipClicked = false
var energizersClicked = false
var selectAllClicked = false
var timeOneClicked = false
var timeTwoClicked = false
var timeThreeClicked = false
var timeFourClicked = false
var groupOneClicked = false
var groupTwoClicked = false
var groupThreeClicked = false

let action = document.getElementById('action')
let inovation = document.getElementById('inovation')
let team = document.getElementById('team')
let leadership = document.getElementById('leadership')
let energizers = document.getElementById('energizers')
let selectAll = document.getElementById('selectAll')
let timeOne = document.getElementById('timeOne')
let timeTwo = document.getElementById('timeTwo')
let timeThree = document.getElementById('timeThree')
let timeFour = document.getElementById('timeFour')
let groupOne = document.getElementById('groupOne')
let groupTwo = document.getElementById('groupTwo')
let groupThree = document.getElementById('groupThree')


function displayNone(element) {
    element.style.display = 'none'
}

function displayBlock(element) {
    element.style.display = 'block'
}

action.addEventListener('click', function() {
    if (actionClicked == false) {
        actionClicked = true
        action.style.backgroundColor = 'rgba(224, 224, 224)';
        action.style.color = 'darkred';

    } else {
        actionClicked = false;
        action.style.backgroundColor = 'white';
        action.style.color = "#4c1a9286";
    }
})
inovation.addEventListener('click', function() {
    if (inovationClicked == false) {
        inovationClicked = true
        inovation.style.backgroundColor = 'rgba(224, 224, 224)';
        inovation.style.color = 'darkred';

    } else {
        inovationClicked = false;
        inovation.style.backgroundColor = 'white';
        inovation.style.color = "#4c1a9286";
    }
})
team.addEventListener('click', function() {
    if (teamClicked == false) {
        teamClicked = true
        team.style.backgroundColor = 'rgba(224, 224, 224)';
        team.style.color = 'darkred';

    } else {
        teamClicked = false;
        team.style.backgroundColor = 'white';
        team.style.color = "#4c1a9286";
    }

})
leadership.addEventListener('click', function() {
    if (leadershipClicked == false) {
        leadershipClicked = true
        leadership.style.backgroundColor = 'rgba(224, 224, 224)';
        leadership.style.color = 'darkred';

    } else {
        leadershipClicked = false;
        leadership.style.backgroundColor = 'white';
        leadership.style.color = "#4c1a9286";
    }
})
energizers.addEventListener('click', function() {
    if (energizersClicked == false) {
        energizersClicked = true
        energizers.style.backgroundColor = 'rgba(224, 224, 224)';
        energizers.style.color = 'darkred';

    } else {
        energizersClicked = false;
        energizers.style.backgroundColor = 'white';
        energizers.style.color = "#4c1a9286";
    }
})
selectAll.addEventListener('click', function() {
    if (selectAllClicked == false) {
        selectAllClicked = true
        selectAll.style.backgroundColor = 'rgba(224, 224, 224)';
        selectAll.style.color = 'darkred';

    } else {
        selectAllClicked = false;
        selectAll.style.backgroundColor = 'white';
        selectAll.style.color = "#4c1a9286";
    }
})
timeOne.addEventListener('click', function() {
    if (timeOneClicked == false) {
        timeOneClicked = true
        timeOne.style.backgroundColor = 'rgba(224, 224, 224)';
        timeOne.style.color = 'darkred';

    } else {
        timeOneClicked = false;
        timeOne.style.backgroundColor = 'white';
        timeOne.style.color = "#4c1a9286";
    }
})
timeTwo.addEventListener('click', function() {
    if (timeTwoClicked == false) {
        timeTwoClicked = true
        timeTwo.style.backgroundColor = 'rgba(224, 224, 224)';
        timeTwo.style.color = 'darkred';

    } else {
        timeTwoClicked = false;
        timeTwo.style.backgroundColor = 'white';
        timeTwo.style.color = "#4c1a9286";
    }
})
timeThree.addEventListener('click', function() {
    if (timeThreeClicked == false) {
        timeThreeClicked = true
        timeThree.style.backgroundColor = 'rgba(224, 224, 224)';
        timeThree.style.color = 'darkred';

    } else {
        timeThreeClicked = false;
        timeThree.style.backgroundColor = 'white';
        timeThree.style.color = "#4c1a9286";
    }
})
timeFour.addEventListener('click', function() {
    if (timeFourClicked == false) {
        timeFourClicked = true
        timeFour.style.backgroundColor = 'rgba(224, 224, 224)';
        timeFour.style.color = 'darkred';
    } else {
        timeFourClicked = false;
        timeFour.style.backgroundColor = 'white';
        timeFour.style.color = "#4c1a9286";
    }
})
groupOne.addEventListener('click', function() {
    if (groupOneClicked == false) {
        groupOneClicked = true
        groupOne.style.backgroundColor = 'rgba(224, 224, 224)';
        groupOne.style.color = 'darkred';
    } else {
        groupOneClicked = false;
        groupOne.style.backgroundColor = 'white';
        groupOne.style.color = "#4c1a9286";
    }
})
groupTwo.addEventListener('click', function() {
    if (groupTwoClicked == false) {
        groupTwoClicked = true
        groupTwo.style.backgroundColor = 'rgba(224, 224, 224)';
        groupTwo.style.color = 'darkred';
    } else {
        groupTwoClicked = false;
        groupTwo.style.backgroundColor = 'white';
        groupTwo.style.color = "#4c1a9286";
    }
})
groupThree.addEventListener('click', function() {
    if (groupThreeClicked == false) {
        groupThreeClicked = true
        groupThree.style.backgroundColor = 'rgba(224, 224, 224)';
        groupThree.style.color = 'darkred';
    } else {
        groupThreeClicked = false;
        groupThree.style.backgroundColor = 'white';
        groupThree.style.color = "#4c1a9286";
    }
})

var categories = []
var timeframe = []
var groupsize = []

function getCategories() {
    categories = []
    if (selectAllClicked) {
        categories.push('.action', '.inovation', '.leadership', '.team', '.energizers')
        return categories
    }
    if (!actionClicked && !inovationClicked && !leadershipClicked && !teamClicked && !energizersClicked) {
        categories.push('.action', '.inovation', '.leadership', '.team', '.energizers')
        return categories
    }
    if (actionClicked)
        categories.push('.action')
    if (inovationClicked)
        categories.push('.inovation')
    if (leadershipClicked)
        categories.push('.leadership')
    if (teamClicked)
        categories.push('.team')
    if (energizersClicked)
        categories.push('.energizers')

    return (categories)
}

function getTimeframes() {
    timeframe = []
    if (!timeOneClicked && !timeTwoClicked && !timeThreeClicked && !timeFourClicked) {
        timeframe.push('.timeOne', '.timeTwo', '.timeThree', '.timeFour')
        return timeframe
    }
    if (timeOneClicked)
        timeframe.push('.timeOne')
    if (timeTwoClicked)
        timeframe.push('.timeTwo')
    if (timeThreeClicked)
        timeframe.push('.timeThree')
    if (timeFourClicked)
        timeframe.push('.timeFour')
    return timeframe
}

function getGroups() {
    groupsize = []
    if (!groupOneClicked && !groupTwoClicked && !groupThreeClicked) {
        groupsize.push('.groupOne', '.groupTwo', '.groupThree')
        return groupsize
    }
    if (groupOneClicked)
        groupsize.push('.groupOne')
    if (groupTwoClicked)
        groupsize.push('.groupTwo')
    if (groupThreeClicked)
        groupsize.push('.groupThree')
    return groupsize
}
var pair = []
var selected = []

function getPairs() {
    categories = getCategories()
    timeframe = getTimeframes()
    groupsize = getGroups()
    selected = []
    for (i = 0; i < categories.length; i++) {
        pair = [categories[i]]
        for (j = 0; j < timeframe.length; j++) {
            pair.push(timeframe[j])
            for (y = 0; y < groupsize.length; y++) {
                pair.push(groupsize[y])
                selected.push(pair.join(""))
                pair.pop()
            }
            pair.pop()
        }
        pair.pop()
    }
    return selected
}
var pairsSelected = []
var elementsSelected = []
var el

function getElements() {
    pairsSelected = getPairs()
    elementsSelected = []
    for (i = 0; i < pairsSelected.length; i++) {
        el = document.querySelectorAll(pairsSelected[i]);
        if (el.length !== 0) {
            if (elementsSelected.length == 0)
                elementsSelected.push(el)
            else {
                for (j = 0; j < elementsSelected.length; j++) {
                    if (el !== elementsSelected[j]) {
                        elementsSelected.push(el)
                    }
                }
            }
        }
    }
    return elementsSelected
}

let filterMobile = document.getElementById("filterMobile");


let closeFilters = document.getElementsByClassName('closeFilters')[0];
let cardRow = document.getElementsByClassName('cardFilter')[0]


function searchFunction() {
    allCategories.forEach(displayNone)
    let alert = document.getElementById('noCards');
    alert.style.display = 'none'
    elements = getElements()
    if (elements.length == 0)
        alert.style.display = 'block'

    for (e = 0; e < elements.length; e++) {
        elements[e].forEach(displayBlock)
    }

    if (filterMobile.style.display == 'none') {
        $('.cardFilter').toggle('slow', function() {
            cardRow.style.display = "none";
        });
        $('#filterMobile').slideToggle('slow', function() {
            filterMobile.style.display = "block";
        })
    }
}

let search = document.getElementById('searchFilters')
search.addEventListener('click', searchFunction)

filterMobile.addEventListener('click', function() {
    let cardRow = document.getElementsByClassName('cardFilter')[0]
    $('.cardFilter').toggle("slow", function() {
        cardRow.style.display = "block";
    });
    filterMobile.style.display = "none";
})

closeFilters.addEventListener('click', function() {
    $('.cardFilter').toggle('slow', function() {
        cardRow.style.display = "none";
    });
    $('#filterMobile').slideToggle('slow', function() {
        filterMobile.style.display = "block";
    })
})