<?php
if($_SERVER['REQUEST_METHOD'] != 'POST'){
    header('Location:../public/index.php');
}

require '../app/ValidateRegister.class.php';

$admin = new ValidateRegister();
$admin->validation();

$passhash = md5($_POST['password']);
$post = ['name' => $_POST['name'], 'email' => $_POST['email'], 'password' => $passhash, 'phone_number' => $_POST['phone_number']];

if(isset($_POST['company']) && $_POST['company']!=="")
$post += array('company' => $_POST['company']);

if(isset($_POST['sector_id']) && $_POST['sector_id']!=="")
$post += array('sector_id' => $_POST['sector_id']);

if(isset($_POST['message']) && $_POST['message']!=="")
$post += array('message' => $_POST['message']);

if(isset($_POST['number_of_employees_id']) && $_POST['number_of_employees_id']!=="")
$post += array('number_of_employees_id' => $_POST['number_of_employees_id']);

$query->insert('admin', $post);

header('Location:../public/index.php?registered');
die();

